package ru.ilinovsg.tm.controller;

import ru.ilinovsg.tm.entity.Task;
import ru.ilinovsg.tm.exception.ProjectNotFoundException;
import ru.ilinovsg.tm.exception.TaskNotFoundException;
import ru.ilinovsg.tm.service.ProjectTaskService;
import ru.ilinovsg.tm.service.TaskService;
import ru.ilinovsg.tm.service.UserProjectTaskService;

import java.util.List;

public class TaskController extends AbstractController{

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    private final UserProjectTaskService userProjectTaskService;

    public TaskController(TaskService taskService, ProjectTaskService projectTaskService, UserProjectTaskService userProjectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
        this.userProjectTaskService = userProjectTaskService;
    }

    public int createTask() {
        System.out.println("[Create task]");
        System.out.println("[Please, enter task name]");
        final String name = scanner.nextLine();
        taskService.create(name);
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByName() throws TaskNotFoundException {
        System.out.println("[Update task]");
        System.out.println("[Please, enter task name]");
        final String name = scanner.nextLine();
        final Task task = taskService.findByName(name);
        if(task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[Please, enter task name]");
        final String new_name = scanner.nextLine();
        taskService.update(task.getId(), new_name);
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskById() throws TaskNotFoundException {
        System.out.println("[Update task]");
        System.out.println("[Please, enter task id]");
        final Long id = Long.parseLong(scanner.nextLine());
        final Task task = taskService.findById(id);
        if(task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[Please, enter task name]");
        final String name = scanner.nextLine();
        taskService.update(task.getId(), name);
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByIndex() throws TaskNotFoundException {
        System.out.println("[Update project]");
        System.out.println("[Please, enter task index]");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task = taskService.findByIndex(index);
        if(task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[Please, enter task name]");
        final String name = scanner.nextLine();
        taskService.update(task.getId(), name);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByName() {
        System.out.println("[Please, enter task name]");
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(name);
        if(task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTaskById() throws TaskNotFoundException {
        System.out.println("[Please, enter task id]");
        final Long id = scanner.nextLong();
        final Task task;
        task = taskService.removeById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByIndex() {
        System.out.println("[Please, enter task index]");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.removeByIndex(index);
        if(task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int clearTask() {
        System.out.println("[Clear task]");
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public void viewTask(final Task task) {
        if(task == null) return;
        System.out.println("[View task]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    public int viewTaskByIndex() {
        System.out.println("Enter task index");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.findByIndex(index);
        viewTask(task);
        return 0;
    }

    public int viewTaskByName() {
        System.out.println("Enter task name");
        final String name = scanner.nextLine();
        final Task task = taskService.findByName(name);
        viewTask(task);
        return 0;
    }

    public int viewTaskById() throws TaskNotFoundException {
        System.out.println("Enter task id");
        final Long id = scanner.nextLong();
        final Task task = taskService.findById(id);
        viewTask(task);
        return 0;
    }

    public int listTask() {
        int index = 1;
        System.out.println("[List task]");
        viewTasks(taskService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    public void viewTasks(final List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        int index = 1;
        for(final Task task: tasks) {
            if (task.getUserId() == null)
                System.out.println(index + "." + task.getId() + " " + task.getName());
            else
                System.out.println(index + "." + task.getId() + " " + task.getName() + " " + task.getUserId());
            index++;
        }
    }

     public int listTaskByProjectId() {
         System.out.println("[List task by project id]");
         System.out.println("[Please, enter project id]");
         final Long projectId = Long.parseLong(scanner.nextLine());
         final List<Task> tasks = taskService.findAllByProjectId(projectId);
         viewTasks(tasks);
         System.out.println("[OK]");
         return 0;
     }

     public int addTaskToProjectByIds() throws ProjectNotFoundException, TaskNotFoundException {
         System.out.println("[Add task to project by ids]");
         System.out.println("[Please, enter project id]");
         final Long projectId = Long.parseLong(scanner.nextLine());
         System.out.println("[Please, enter task id]");
         final Long taskId = Long.parseLong(scanner.nextLine());
         projectTaskService.addTaskToProject(projectId, taskId);
         System.out.println("[OK]");
         return 0;
     }

     public int removeTaskFromProjectByIds() {
         System.out.println("[Remove task from project by id]");
         System.out.println("[Please, enter project id]");
         final Long projectId = Long.parseLong(scanner.nextLine());
         System.out.println("[Please, enter task id]");
         final Long taskId = Long.parseLong(scanner.nextLine());
         projectTaskService.removeTaskFromProject(projectId, taskId);
         System.out.println("[OK]");
         return 0;
    }

    public int removeTasksAndProject() throws ProjectNotFoundException {
        System.out.println("[Remove tasks and project by id]");
        System.out.println("[Please, enter project id]");
        final Long projectId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTasksAndProject(projectId);
        System.out.println("[OK]");
        return 0;
    }

    public int listTaskByUserId() {
        System.out.println("[List task by user id]");
        System.out.println("[Please, enter user id]");
        final Long userId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = taskService.findAllByUserId(userId);
        viewTasks(tasks);
        System.out.println("[OK]");
        return 0;
    }

    public int addTaskToUserByIds() throws TaskNotFoundException {
        System.out.println("[Add task to user by ids]");
        System.out.println("[Please, enter user id]");
        final Long userId = Long.parseLong(scanner.nextLine());
        System.out.println("[Please, enter task id]");
        final Long taskId = Long.parseLong(scanner.nextLine());
        userProjectTaskService.addTaskToUser(userId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskFromUserByIds() {
        System.out.println("[Remove task from user by id]");
        System.out.println("[Please, enter user id]");
        final Long userId = Long.parseLong(scanner.nextLine());
        System.out.println("[Please, enter task id]");
        final Long taskId = Long.parseLong(scanner.nextLine());
        userProjectTaskService.removeTaskFromUser(userId, taskId);
        System.out.println("[OK]");
        return 0;
    }

}

